from hashlib import sha256
from urllib.parse import urljoin

import requests
from django.conf import settings

from django_cdstack_models.django_cdstack_models.models import CmdbHost


def wazuh_get_or_create_host(cmdb_host: CmdbHost):
    password_key = sha256(cmdb_host.api_key.key.encode("utf-8")).hexdigest()

    session = requests.Session()
    session.headers = {
        "Accept": "application/json",
    }

    # Host Object anlegen
    session.auth = (settings.WAZUH_API_USER, settings.WAZUH_API_PASSWD)

    request_url = urljoin(settings.WAZUH_API_URL, "agents/insert")

    agent_data = dict()
    agent_data["id"] = str(cmdb_host.id)
    agent_data["name"] = cmdb_host.monitoring_id
    agent_data["ip"] = "any"
    agent_data["key"] = password_key

    request_args = {"url": request_url, "json": agent_data, "verify": False}

    response = session.post(**request_args)
    data = response.json()

    if response.status_code in (201, 200):
        return data["data"]["key"]

    return False
